type IngredientStore = {
  id: number
  name: string
  unit: string
  price: number
}

export { type IngredientStore }
