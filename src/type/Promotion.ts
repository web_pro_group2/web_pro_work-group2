type Promotion = {
  id: number
  condition: string
  startDate: Date
  endDate: Date
}

export type { Promotion }
