type DetailCheckStocks = {
  id: number
  Date: string
  ProductCode: string
  Product: string
  Price: number
  Min: number
  Balance: number
  Unit: string
}
export type { DetailCheckStocks }
