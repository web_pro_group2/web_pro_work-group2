import type { Status } from './Status'

type Branch = {
  id: number
  address: string
  status: Status
}

export type { Branch }
