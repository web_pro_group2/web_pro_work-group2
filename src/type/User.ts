import type { Role } from "./Role"

interface User {
  id?: number
  email: string
  password: string
  fullName: string
  tel: string
  image: string
  roles: Role[]
}
export type { User }