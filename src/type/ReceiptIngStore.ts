import type { ReceiptItemIng } from './ReceiptItemIng'

type ReceiptIngStore = {
  id: number
  createdDate: Date
  totalBefore: number
  // memberDiscount: number
  total: number
  totalAmount: number
  receivedAmount: number
  change: number
  paymentType: string
  //userId: number;
  // user?: User;
  // memberId: number;
  // member?: Member;
  receiptIngredientItem?: ReceiptItemIng[]
}

export type { ReceiptIngStore }
