import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/login' // Redirect to the login view by default
    },
    {
      path: '/home',
      name: 'home',
      components: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POS/PosView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checkStock',
      name: 'checkStock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/ShowCheckStockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/ingStore',
      name: 'ingStore',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/IngredientStore/IngredientsStore.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/check',
      name: 'check',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckStock/CheckStock.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/detailcheckStock',
      name: 'detailcheckStock',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POS/DetailCheckStockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/purchase',
      name: 'purchase',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CheckStock/PurchaseOrder.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Login/views/LoginView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'FullLayout',
        requireAuth: true
      }
    },
    {
      path: '/credit',
      name: 'credit',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/CreditView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/member',
      name: 'member',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/MemberView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotions',
      name: 'promotions',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Promotion/PromotionView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/products',
      name: 'products',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Product/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/branch',
      name: 'branch',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Branch/BranchView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    }
  ]
})

// function isLogin() {
//   const user = localStorage.getItem('user')
//   if (user) {
//     return true
//   }
//   return false
// }

// router.beforeEach((to, from) => {
//   if ((to.meta.requireAuth && !isLogin())) {
//     router.replace('/login')
//   }
// })

export default router
