import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/type/User'
import { useRouter } from 'vue-router'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import authService from '../services/auth'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()

  const login = async (email: string, password: string) => {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token))
      messageStore.showMessage('Login Success')
      router.replace('/pos')
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }

  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  // function checkUser(email: string, password: string): boolean {
  //   for (const user of userDataStore.users)
  //     if (user.email === email && user.password === password) {
  //       currentUser.value = user
  //       router.push('/home')
  //       showDrawer.value = true
  //       console.log(currentUser.value)
  //       return true
  //     }
  //   console.log('Authentication failed')
  //   return false
  // }
  // function logout(): void {
  //   currentUser.value = userDataStore.defaultUser
  //   router.push('/login')
  //   showDrawer.value = false
  // }

  return {
    // currentUser,
    // isAuthenticated,
    // checkUser,
    login,
    logout,
    getCurrentUser,
    getToken
  }
})
