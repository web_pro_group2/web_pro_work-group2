import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
import type { Member } from '@/type/Member'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const members = ref<Member[]>([])

  const statusMember = ref(false)
  const currentMember = ref<Member | null>()

  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
    statusMember.value = true
  }

  const clearCurrentMember = () => {
    currentMember.value = null
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    members.value = res.data
    loadingStore.finish()
  }

  //data function
  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember(member: Member) {
    loadingStore.doLoad()
    if (member.id < 0) {
      //add new
      console.log('post ' + JSON.stringify(member))
      const res = await memberService.addMember(member)
    } else {
      //update
      console.log('patch ' + JSON.stringify(member))
      const res = await memberService.updateMember(member)
    }
    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember(member: Member) {
    loadingStore.doLoad()
    const res = await memberService.removeMember(member)
    await getMembers()
    loadingStore.finish()
  }

  return {
    members,
    currentMember,
    statusMember,
    searchMember,
    clearCurrentMember,
    getMember,
    getMembers,
    saveMember,
    deleteMember
  }
})
