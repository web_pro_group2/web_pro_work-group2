import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import statusService from '@/services/status'
import type { Status } from '@/type/Status'

export const useStatusStore = defineStore('status', () => {
  const loadingStore = useLoadingStore()
  const status = ref<Status[]>([])
  const initialStatus: Status = {
    name: ''
  }
  const editedStatus = ref<Status>(JSON.parse(JSON.stringify(initialStatus)))

  async function getStatus(id: number) {
    loadingStore.doLoad()
    const res = await statusService.getStatus(id)
    editedStatus.value = res.data
    loadingStore.finish()
  }
  async function getStatuss() {
    try {
      loadingStore.doLoad()
      const res = await statusService.getStatuss()
      status.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveStatus() {
    loadingStore.doLoad()
    const status = editedStatus.value
    if (!status.id) {
      // Add new
      console.log('Post ' + JSON.stringify(status))
      const res = await statusService.addStatus(status)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(status))
      const res = await statusService.updateStatus(status)
    }

    await getStatuss()
    loadingStore.finish()
  }
  async function deleteStatus() {
    loadingStore.doLoad()
    const status = editedStatus.value
    const res = await statusService.delStatus(status)

    await getStatuss()
    loadingStore.finish()
  }

  function clearForm() {
    editedStatus.value = JSON.parse(JSON.stringify(initialStatus))
  }
  return {
    status,
    getStatuss,
    saveStatus,
    deleteStatus,
    initialStatus,
    editedStatus,
    getStatus,
    clearForm
  }
})
