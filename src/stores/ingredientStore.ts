import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { IngredientStore } from '@/type/IngredientStore'

export const useIngredientStore = defineStore('ingredient', () => {
  const ingredients = ref<IngredientStore[]>([
    { id: 1, name: 'ผงชาเขียว', unit: 'กิโลกรัม', price: 50 },
    { id: 2, name: 'ผงโกโก้', unit: 'กิโลกรัม', price: 50 },
    { id: 3, name: 'น้ำตาล', unit: 'กิโลกรัม', price: 50 },
    { id: 4, name: 'นมข้น', unit: 'กิโลกรัม', price: 50 },
    { id: 5, name: 'ครีมเทียม', unit: 'กิโลกรัม', price: 80 },
    { id: 6, name: 'น้ำเชื่อม', unit: 'กิโลกรัม', price: 40 },
    { id: 7, name: 'วิปครีม', unit: 'กิโลกรัม', price: 40 },
    { id: 8, name: 'ไซรัป', unit: 'กิโลกรัม', price: 40 },
    { id: 9, name: 'น้ำแข็ง', unit: 'กิโลกรัม', price: 40 },
    { id: 10, name: 'แก้ว', unit: 'กิโลกรัม', price: 40 },
    { id: 11, name: 'หลอด', unit: 'กิโลกรัม', price: 40 },
    { id: 12, name: 'เส้นสปสเก็ตตี้', unit: 'กิโลกรัม', price: 40 },
    { id: 13, name: 'ผงกาแฟ', unit: 'กิโลกรัม', price: 40 },
    { id: 14, name: 'ไข่มุก', unit: 'กิโลกรัม', price: 40 },
    { id: 15, name: 'ส้ม', unit: 'กิโลกรัม', price: 40 },
    { id: 16, name: 'บลูเบอร์รี่', unit: 'กิโลกรัม', price: 40 },
    { id: 17, name: 'ช็อคโกแลต', unit: 'กิโลกรัม', price: 40 },
    { id: 18, name: 'สตอเบอร์รี่', unit: 'กิโลกรัม', price: 40 },
    { id: 19, name: 'มะพร้าว', unit: 'กิโลกรัม', price: 40 },
    { id: 20, name: 'แป้ง', unit: 'กิโลกรัม', price: 40 },
    { id: 21, name: 'มะละกอ', unit: 'กิโลกรัม', price: 40 },
    { id: 22, name: 'ข้าวหอมมะลิ', unit: 'กิโลกรัม', price: 40 },
    { id: 23, name: 'ข้าวเหนียว', unit: 'กิโลกรัม', price: 40 },
    { id: 24, name: 'กุ้งสด', unit: 'กิโลกรัม', price: 40 },
    { id: 25, name: 'ไข่ไก่', unit: 'กิโลกรัม', price: 40 },
    { id: 26, name: 'มะเขือเทศ', unit: 'กิโลกรัม', price: 40 },
    { id: 27, name: 'ต้นหอม', unit: 'กิโลกรัม', price: 40 }
  ])

  return { ingredients }
})
