// import { ref } from 'vue'
// import { defineStore } from 'pinia'
// import { useLoadingStore } from './loading'
// import userService from '@/services/user'
// import type { User } from '@/type/User'

// export const useUserDataStore = defineStore('user', () => {
//   const loadingStore = useLoadingStore()
//   const users = ref<User[]>([
//     {
//       id: 1,
//       username: 'admin',
//       password: 'pass1',
//       fullName: 'admin1',
//       position: 'Manager',
//       tel: '084-164-2293'
//     }
//   ])

//   async function getUser(id: number) {
//     loadingStore.doLoad()
//     const res = await userService.getUser(id)
//     users.value = res.data
//     loadingStore.finish()
//   }

//   async function getUsers() {
//     loadingStore.doLoad()
//     const res = await userService.getUsers()
//     users.value = res.data
//     loadingStore.finish()
//   }

//   async function saveUser(user: User) {
//     loadingStore.doLoad()
//     if (user.id < 0) {
//       //add new
//       console.log('post ' + JSON.stringify(user))
//       const res = await userService.addUser(user)
//     } else {
//       //update
//       console.log('patch ' + JSON.stringify(user))
//       const res = await userService.updateUser(user)
//     }
//     await getUsers()
//     loadingStore.finish()
//   }

//   async function deleteUser(user: User) {
//     loadingStore.doLoad()
//     const res = await userService.removeUser(user)
//     await getUsers()
//     loadingStore.finish()
//   }

//   return {
//     users,
//     getUser,
//     getUsers,
//     saveUser,
//     deleteUser
//   }
// })
