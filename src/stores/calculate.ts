import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/type/ReceiptItem'
import type { Product } from '@/type/Product'
import type { Receipt } from '@/type/ReceiptPOS'
import { useMemberStore } from './member'
import { useAuthStore } from './auth'
import orderService from '@/services/order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
export const useCalculateStore = defineStore('calculate', () => {
    const receiptDialog = ref(false)
    const memberDialog = ref(false)
    const searchDialog = ref(false)
    const cashDialog = ref(false)
    const prompayDialog = ref(false)
    const memberStore = useMemberStore()
    const authStore = useAuthStore()
    const loadingStore = useLoadingStore()
    const messageStore = useMessageStore()
    const receiptItems = ref<ReceiptItem[]>([])
    const receiptPos = ref<Receipt>()

    initReceipt()
    function initReceipt() {
      receiptPos.value = {
        id: 0,
        createdDate: new Date(),
        memberDiscount: 0,
        totalBefore: 0,
        total: 0,
        totalAmount: 0,
        receivedAmount: 0,
        change: 0,
        paymentType: 'cash',
        userId: authStore.getCurrentUser()!.id!,
        user: authStore.getCurrentUser()!,
        memberId: -1
      }
      receiptItems.value = []
    }


    watch(receiptItems, () => {
      calReceipt()
    },
      { deep: true }
    )
  
    function addReceiptItem(product: Product) {
      const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
      console.log(receiptItems)
      if (index >= 0) {
        receiptItems.value[index].unit++
        calReceipt()
        return
      } else {
        const newReceipt: ReceiptItem = {
          id: -1,
          name: product.name,
          price: product.price,
          unit: 1,
          productId: product.id!,
          product: product
        }
        receiptItems.value.push(newReceipt)
        calReceipt()
      }
    }
    function deleteReceiptItem(receiptItem: ReceiptItem) {
      const index = receiptItems.value.findIndex((item) => item === receiptItem)
      receiptItems.value.splice(index, 1)
      calReceipt()
    }
    function inc(item: ReceiptItem) {
      item.unit++
      calReceipt()
    }
    function dec(item: ReceiptItem) {
      if (item.unit === 1) {
        deleteReceiptItem(item)
      } else {
        item.unit--
        calReceipt()
      }
    }

    const order = async () => {
      try {
        loadingStore.doLoad()
        await orderService.addOrder(receiptPos.value!, receiptItems.value)
        initReceipt()
        loadingStore.finish()
      } catch (e: any) {
        messageStore.showMessage(e.message)
        loadingStore.finish()
      }
    }
  
    function showReceiptDialog() {
      receiptDialog.value = true
    }
  
    function showSearchDialog() {
      searchDialog.value = true
    }
  
    function showCashDialog() {
      cashDialog.value = true
    }
    function showPrompayDialog() {
      prompayDialog.value = true
    }
  
    function showmemberDialog() {
      memberDialog.value = true
    }
  
    function save(Amount: number) {
      receiptPos.value!.receivedAmount = Amount
      receiptPos.value!.change = Amount - receiptPos.value!.total
      cashDialog.value = false
      // productStore.clear()
    }
  
    function searchItem(Word: string) {
      const sanitizedQuery = Word.toLowerCase()
      const searchResults = receiptItems.value
        .filter((receiptItem) => receiptItem.product?.name.toLowerCase().includes(sanitizedQuery))
        .map((receiptItem) => receiptItem.product!)
        return searchResults
    }
  
    function clear() {
      receiptItems.value = []
      receiptPos.value!.totalAmount = 0
      receiptPos.value!.receivedAmount = 0
      receiptPos.value!.totalBefore = 0
      receiptPos.value!.total = 0
      receiptPos.value!.change = 0
      receiptPos.value!.memberDiscount = 0
      receiptPos.value!.member = undefined
      receiptPos.value!.memberId = -1
      memberStore.statusMember = false
    }
  
    function calReceipt() {
      let totalBefore = 0
      let amount = 0
      let discount = 0
      for (const item of receiptItems.value) {
        totalBefore = totalBefore + item.price * item.unit
        amount = amount + item.unit
      }
  
      receiptPos.value!.totalAmount = amount
      discount = totalBefore / 10
  
      receiptPos.value!.totalBefore = totalBefore
  
      if (memberStore.statusMember) {
        receiptPos.value!.total = totalBefore - discount
        receiptPos.value!.memberDiscount = discount
      } else {
        receiptPos.value!.total = totalBefore
      }
    }

  return { receiptPos, receiptItems, cashDialog, memberDialog, searchDialog, prompayDialog, receiptDialog,
    addReceiptItem, deleteReceiptItem, inc, dec, showCashDialog, showPrompayDialog, showReceiptDialog, showSearchDialog, showmemberDialog, save, searchItem, clear, order }
})


