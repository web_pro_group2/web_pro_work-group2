import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
//mport type { ReceiptItem } from '@/type/ReceiptItemIng'
import type { ReceiptIngStore } from '@/type/ReceiptIngStore'
import type { ReceiptItemIng } from '@/type/ReceiptItemIng'
import { useLoadingStore } from './loading'
import orderIngredientService from '@/services/orderIngredient'

export const useReceiptIngStore = defineStore('receipt', () => {
  //const authStore = useAuthStore()
  //const memberStore = useMemberStore()
  const loadingStore = useLoadingStore()
  const receiptIngDialog = ref(false)
  const receiptItemsIngredient = ref<ReceiptItemIng[]>([])
  const receiptIng = ref<ReceiptIngStore>()
  initReceipt()

  function initReceipt() {
    receiptIng.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      total: 0,
      totalAmount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: ''
      //userId: authStore.getCurrentUser()!.id!,
    }
    receiptItemsIngredient.value = []
  }

  watch(
    receiptItemsIngredient,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const calReceipt = function () {
    receiptIng.value!.total = 0
    receiptIng.value!.totalAmount = 0
    for (let i = 0; i < receiptItemsIngredient.value.length; i++) {
      receiptIng.value!.total +=
        receiptItemsIngredient.value[i].price * receiptItemsIngredient.value[i].qty
      receiptIng.value!.totalAmount += receiptItemsIngredient.value[i].qty
      console.log('cal')
    }
  }

  const addReceiptItem = (newReceiptItemIngredient: ReceiptItemIng) => {
    receiptItemsIngredient.value.push(newReceiptItemIngredient)
    console.log('add Item')
  }
  const deleteReceiptItem = (selectedItem: ReceiptItemIng) => {
    const index = receiptItemsIngredient.value.findIndex((item) => item === selectedItem)
    receiptItemsIngredient.value.splice(index, 1)
    console.log('del')
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItemIng) => {
    selectedItem.qty++
    calReceipt()
    console.log('+')
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItemIng) => {
    selectedItem.qty--
    if (selectedItem.qty === 0) {
      deleteReceiptItem(selectedItem)
      console.log('-')
    }
  }
  const removeItem = (item: ReceiptItemIng) => {
    const index = receiptItemsIngredient.value.findIndex((ri) => ri === item)
    receiptItemsIngredient.value.splice(index, 1)
  }

  const order = async () => {
    try {
      loadingStore.doLoad()
      await orderIngredientService.addOrder(receiptIng.value!, receiptItemsIngredient.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      //messageStore.showMessage(e.message)
    }
  }

  function showReceiptIngDialog() {
    //receiptIng.value.receiptItemsIng = receiptItemsIng.value
    receiptIngDialog.value = true
  }

  return {
    receiptIng,
    receiptItemsIngredient,
    receiptIngDialog,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    calReceipt,
    removeItem,
    order,
    showReceiptIngDialog,
    initReceipt
  }
})
