import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
// import userService from '@/services/user'
// import type { User } from '@/types/User'
import promotionService from '@/services/promotion'
import type { Promotion } from '@/type/Promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])

  async function getPromotion(id: number) {
    loadingStore.doLoad()
    const res = await promotionService.getUPromotion(id)
    promotions.value = res.data
    loadingStore.finish()
  }

  //data function
  async function getPromotions() {
    loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }

  async function savePromotion(promotion: Promotion) {
    loadingStore.doLoad()
    if (promotion.id < 0) {
      //add new
      console.log('post ' + JSON.stringify(promotion))
      const res = await promotionService.addPromotion(promotion)
    } else {
      //update
      console.log('patch ' + JSON.stringify(promotion))
      const res = await promotionService.updatePromotion(promotion)
    }
    await getPromotions()
    loadingStore.finish()
  }

  async function deletePromotion(promotion: Promotion) {
    loadingStore.doLoad()
    const res = await promotionService.removePromotion(promotion)
    await getPromotions()
    loadingStore.finish()
  }
  return { promotions, getPromotions, savePromotion, deletePromotion, getPromotion }
})
