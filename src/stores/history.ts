// store/history.ts
import http from '@/services/http'
import { defineStore } from 'pinia'

export const useHistory = defineStore('history', () => {
  function getCheckStockHistory() {
    return http.get('/history')
  }

  return { getCheckStockHistory }
})
