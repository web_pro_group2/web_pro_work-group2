import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
// import userService from '@/services/user'
// import type { User } from '@/types/User'
import branchService from '@/services/branch'
import type { Branch } from '@/type/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branchs = ref<Branch[]>([])
  const initilBranch: Branch = {
    id: -1,
    address: '',
    status: { id: 2, name: 'On Hold' }
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initilBranch)))

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    branchs.value = res.data
    loadingStore.finish()
  }

  //data function
  async function getBranchs() {
    loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    loadingStore.finish()
  }

  async function saveBranch(branch: Branch) {
    loadingStore.doLoad()
    if (branch.id < 0) {
      //add new
      console.log('post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      //update
      console.log('patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }
    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch(branch: Branch) {
    loadingStore.doLoad()
    const res = await branchService.removeBranch(branch)
    await getBranchs()
    loadingStore.finish()
  }
  return { branchs, getBranchs, saveBranch, deleteBranch, getBranch, initilBranch, editedBranch }
})
