import type { Branch } from '@/type/Branch'
import http from './http'

function addBranch(branch: Branch) {
  return http.post('/branch', branch)
}

function updateBranch(branch: Branch) {
  return http.patch(`/branch/${branch.id}`, branch)
}

function removeBranch(branch: Branch) {
  return http.delete(`/branch/${branch.id}`)
}

function getBranch(id: number) {
  return http.get(`/branch/${id}`)
}

function getBranchs() {
  return http.get('/branch')
}

export default { addBranch, updateBranch, removeBranch, getBranch, getBranchs }
