
import type { Role } from '@/type/Role'
import http from './http'

function addRole(role: Role) {
  return http.post('/role', role)
}

function updateRole(role: Role) {
  return http.patch(`/role/${role.id}`, role)
}

function delRole(role: Role) {
  return http.delete(`/role/${role.id}`)
}

function getRole(id: number) {
  return http.get(`/role/${id}`)
}

function getRoles() {
  return http.get('/role')
}

export default { addRole, updateRole, delRole, getRole, getRoles }
