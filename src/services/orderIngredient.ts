import type { History } from '@/type/History'
import http from './http'
import type { ReceiptIngStore } from '@/type/ReceiptIngStore'
import type { ReceiptItemIng } from '@/type/ReceiptItemIng'

type ReceiptDto = {
  orderIngredientItems: {
    ingredientId: number
    qty: number
  }[]
  //userId: number
}

function addOrder(ReceiptIngStore: ReceiptIngStore, receiptItemsIngredient: ReceiptItemIng[]) {
  const receiptDto: ReceiptDto = {
    orderIngredientItems: []
    //userId: 0
  }
  //receiptDto.userId = receipt.userId
  receiptDto.orderIngredientItems = receiptItemsIngredient.map((item) => {
    return {
      ingredientId: item.ingredientId,
      qty: item.qty
    }
  })
  //console.log(receiptDto)
  console.log('post Order')
  return http.post('/order-ingredient', receiptDto)
}

export default { addOrder }
