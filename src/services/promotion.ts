import type { Promotion } from '@/type/Promotion'
import http from './http'

function addPromotion(promotion: Promotion) {
  return http.post('/promotion', promotion)
}

function updatePromotion(promotion: Promotion) {
  return http.patch(`/promotion/${promotion.id}`, promotion)
}

function removePromotion(promotion: Promotion) {
  return http.delete(`/promotion/${promotion.id}`)
}

function getUPromotion(id: number) {
  return http.get(`/promotion/${id}`)
}

function getPromotions() {
  return http.get('/promotion')
}

export default { addPromotion, updatePromotion, removePromotion, getUPromotion, getPromotions }
