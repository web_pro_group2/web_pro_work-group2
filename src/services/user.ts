
import type { User } from '@/type/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('tel', user.tel)
  formData.append('password', user.password)
  formData.append('roles', JSON.stringify(user.roles))
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post('/user', formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}

function updateUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('tel', user.tel)
  formData.append('password', user.password)
  formData.append('roles', JSON.stringify(user.roles))
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post(`/user/${user.id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  })
}

function delUser(user: User) {
  return http.delete(`/user/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/user/${id}`)
}

function getUsers() {
  return http.get('/user')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
